package main

import "fmt"

func main(){
	//   pointer
	// var a int = 20
	// var pa *int = &a

	//fmt.Println(*pa)

	//       go array slice
	// slice1 := [3]float64{1,2,3}
	// slice2 := slice1[:2]

	// fmt.Println(slice2)

	//      go copy and append

	ar := []int{10, 20, 30}
	ar2 := append(ar, 100, 200, 300)

	fmt.Println(ar2)

	cp := []float64{9, 8, 3, 2}

	cp2 := make([]float64, 2)

	copy(cp2, cp)

	copy := cp2[:]

	fmt.Println(copy)

	
}